﻿using System;
using UnityEngine;
using VoxelBusters.NativePlugins;

namespace LaikaBOSS {
#if USES_GAME_SERVICES
    public class GameServices {
        public static bool IsLogined { get { return NPBinding.GameServices.LocalUser.IsAuthenticated; } }

        public static void OpenAchievementsUI() {
            LoginAndCall(() => { NPBinding.GameServices.ShowAchievementsUI(error => { }); });
        }

        public static void ReportAchievement(string achievement, bool completed) {
            LoginAndCall(
                () => {
                    NPBinding.GameServices.ReportProgressWithID(achievement, completed ? 1 : 0, (success, error) => { });
                });
        }

        public static void ReportLeaderboard(string leaderboard, long value) {
            LoginAndCall(
                () => { NPBinding.GameServices.ReportScoreWithID(leaderboard, value, (success, error) => { }); });
        }

        public static void OpenLeaderboardsUI(string leaderboardId) {
            LoginAndCall(() => {
                NPBinding.GameServices.ShowLeaderboardUIWithGlobalID(leaderboardId, eLeaderboardTimeScope.ALL_TIME,
                    error => { });
            });
        }

        public static void LoadFriends(Action<User[]> callback) {
            LoginAndCall(() => {
                NPBinding.GameServices.LocalUser.LoadFriends((friends, error) => {
                    if (friends != null) callback.CallCallback(friends);
                    else Debug.Log("Failed to load user friends with error " + error);
                });
            });
        }

        //callback is only called on success
        private static void LoginAndCall(Action callback) {
            Login(result => {
                if (result) callback.CallCallback();
                else OneButtonWindow.Show(Localization.Get("ErrorKey"), Localization.Get("ErrorDescriptionKey"));
            });
        }

        //callback is called anyway
        private static void Login(Action<bool> callback) {
            NPBinding.GameServices.LocalUser.Authenticate((success, error) => {
                if (success) Debug.Log("Local User Details : " + NPBinding.GameServices.LocalUser);
                else Debug.Log("Sign-In failed with error " + error);
                callback.CallCallback(success);
            });
        }
    }
#endif
}